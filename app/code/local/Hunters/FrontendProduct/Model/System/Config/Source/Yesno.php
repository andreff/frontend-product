<?php

/**
 * Class Hunters_FrontendProduct_Model_System_Config_Source_Yesno
 */
class Hunters_FrontendProduct_Model_System_Config_Source_Yesno
{
    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label'=>Mage::helper('hunters_frontendproduct')->__('Enabled')),
            array('value' => 0, 'label'=>Mage::helper('hunters_frontendproduct')->__('Disabled')),
        );
    }
}