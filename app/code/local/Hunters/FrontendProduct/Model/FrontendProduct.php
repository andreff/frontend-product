<?php

/**
 * FrontendProduct class model
 * Class Hunters_FrontendProduct_Model_FrontendProduct
 */
class Hunters_FrontendProduct_Model_FrontendProduct  extends Mage_Core_Model_Abstract
{

    const STATUS_ACTIVE = 1;
    const STATUS_CANCEL = 2;
    const STATUS_SUSPENDED = 3;

    /**
     * @var Mage_Catalog_Model_Product
     */
    protected $_productModel;

    /**
     * @var  Mage_Customer_Model_Customer
     */
    protected $_customer;

    protected $_logger;


    protected function _construct()
    {

        $this->_customer = Mage::getSingleton('customer/session')->getCustomer();

        /*
         * use standart 'catalog/product' for operations with product
         */
        $this->_productModel = Mage::getModel('catalog/product');

        /*
         * crosstable to link product and customer
         */
        $this->_init('hunters_frontendproduct/frontendproduct');

        $this->_logger = Mage::helper('hunters_frontendproduct/logger');
    }

    public function setCustomer(Mage_Customer_Model_Customer $customer)
    {
        if((int)$customer->getId() < 1) {
            throw new \Exception('Invalid Customer Model');
        }
        $this->_customer = $customer;

        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setDataFromRequest(array $data)
    {
        $this->setData('data_from_request',$data);

        $this->_productModel->addData($data);

        return $this;
    }


    public function createProduct()
    {

        $result = false;


        try {

            $product = $this->_productModel;

            $product->setAttributeSetId( $this->getAttributeSetId() );
            $product->setTypeId( $this->getDefaultTypeId() );
            $product->setAdType( $this->getDefaultAdType() );
            $product->setSku(time());
            $product->setPrice(0);
            $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
            $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);

            $product->setTaxClassId(0);// # default tax class

            $product->setStockData(array(
                'is_in_stock' => 1,
                'qty' => 99999
            ));

            $product->setWebsiteIDs(array(
                Mage::app()->getWebsite()->getId()
            ));

            $product->setMediaGallery(array('images' => array(), 'values' => array()));
            $this->prepareProductImages();

            $product->save();

            $this->prepareNewProductData($product);

            $this->save();

            $result = true;

            $this->_logger->log(sprintf('New Product (ID %s) created by customer (ID %s) successfuly.',$product->getId(), $this->getCustomerId()));

        } catch (Exception $e) {

            $this->_logger->logException($e);

            Mage::getSingleton('core/session')->addError($e->getMessage());

            Mage::dispatchEvent(
                'frontendproduct_create_fail',
                array('product' => $this->getProduct())
            );

            return false;
        }

        return $result;

    }

    public function updateProduct()
    {

        $result = false;


        try {

            $product = $this->_productModel;

            $product->setData('_edit_mode', true);

            $this->prepareProductImages();

            $product->save();

            $this->save();

            $result = true;

            $this->_logger->log(sprintf('Product (ID %s) saved by customer (ID %s) successfuly.',$product->getId(), $this->getCustomerId()));

        } catch (Exception $e) {

            $this->_logger->logException($e);

            Mage::getSingleton('core/session')->addError($e->getMessage());

            Mage::dispatchEvent(
                'frontendproduct_edit_fail',
                array('product' => $this->getProduct())
            );

            return false;
        }


        return $result;

    }

    protected function getDefaultTypeId()
    {
        return 'virtual';
    }

    protected function getDefaultAdType()
    {

        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'ad_type');

        return $attribute->getDefaultValue();
    }

    protected function getAttributeSetId()
    {
        return 4;
    }

    protected function prepareNewProductData($product)
    {
        $this->setProductId($product->getId())
            ->setCustomerId($this->getCustomerId())
            ->setStatus(self::STATUS_ACTIVE)
            ->setUpdatedAt(time())
            ->setCurrentAmount(0);

        return $this;
    }

    protected function prepareProductImages()
    {

        $imageGallery = $this->getImageDataFromRequest();

        if(count($imageGallery) > 0) {

            $product = $this->getProduct();

            $mediaAttribute = array (
                'image',
                'thumbnail',
                'small_image'
            );

            foreach($imageGallery as $imageInfo) {

                $product->addImageToMediaGallery($imageInfo['path'] . $imageInfo['file'], $mediaAttribute, false, false);

                if(count($mediaAttribute) > 1) {

                    $currentImage = array_pop($product->getMediaGallery()['images']);
                    $product->setImage($currentImage['file']);
                    $product->setSmallImage($currentImage['file']);
                    $product->setThumbnail($currentImage['file']);
                }

                $mediaAttribute = null; #first image set as default

            }

        }

        return $this;

    }

    protected function getCustomerId()
    {
        /**
         * TODO
         * check customer session
         */
        if((bool)($this->_customer instanceof Mage_Customer_Model_Customer) === false) {
            throw new \Exception('Invalid Customer Model');
        }

        $id = $this->_customer->getId();

        if((int)$id < 1) {
            throw new \Exception('Invalid Customer Id');
        }

        return $id;
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }


    public function getCustomer()
    {
        return $this->_customer;
    }

    public function getProduct()
    {
        if((bool)$this->getCurrentAmount() === true){
            $this->_productModel->setCurrenAmount($this->getCurrentAmount());
        }

        return $this->_productModel;
    }

    /**
     * @param $productId
     * @return mixed
     */
    public function loadByProductId($productId)
    {
        $collection = $this->getCollection()
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('customer_id', $this->getCustomer()->getId());

        $this->setData($collection->getFirstItem()->getData());

        return $this;

    }

}
