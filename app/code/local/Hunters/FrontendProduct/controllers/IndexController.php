<?php

/**
 * Class Hunters_FrontendProduct_IndexController
 */
class Hunters_FrontendProduct_IndexController extends Mage_Core_Controller_Front_Action
{

    /**
     * Initialize product from request parameters
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _initFrontendProduct()
    {

        $productId  = (int) $this->getRequest()->getParam('id');
        $frontendProduct = Mage::getModel('hunters_frontendproduct/frontendProduct');
        $product = $frontendProduct->getProduct();
        //$product->setStoreId($this->getRequest()->getParam('store', 0));

        $product->setData('_edit_mode', true);
        if ($productId) {
            try {
                 $frontendProduct->loadByProductId($productId);

                #
                if((int)$frontendProduct->getId() < 1) {
                    throw new \Exception('Invalid product ID');
                }

                $product->load($frontendProduct->getProductId());
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }

        return $frontendProduct;
    }

    public function preDispatch()
    {
        parent::preDispatch();
        $action = $this->getRequest()->getActionName();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    public function paymentAction()
    {
        $redirectUrl = Mage::getUrl('product/index/manage/', array('_secure' => true));
        

        if (!$this->_validateFormKey()) {
            $this->_redirectError($redirectUrl);
            return;
        }

        /** @var $session Mage_Customer_Model_Session */
        $session = $this->_getSession();
        if ( false === (bool)$session->isLoggedIn() ) {
            $this->_redirect('*/*/');
            return;
        }

        if (!$this->getRequest()->isPost()) {
            $this->_redirectError($redirectUrl);
            return;
        }


        $frontendProduct = Mage::helper('hunters_frontendproduct')->initFrontendProduct();

        if((int)$frontendProduct->getProduct()->getId() < 1) {
            $this->_getSession()->addError('Invalid product to edit');
            $this->_redirectError($redirectUrl);
            return;
        }

        $redirectUrl = Mage::getUrl('product/index/edit/id/' . $frontendProduct->getProduct()->getId(), array('_secure' => true));

        $amount = (float)Mage::app()->getRequest()->getPost('amount');

        if((bool)$amount === false) {
            $this->_getSession()->addError('Invalid amount value');
            $this->_redirectError($redirectUrl);
            return;
        }


        if($this->checkCustomerBalance($amount) === false){
            $this->_getSession()->addError('Not enough money');
            $this->_redirectError($redirectUrl);
            return;
        }


        try {

            $frontendProduct->setCurrentAmount($frontendProduct->getCurrentAmount()+$amount);

            $frontendProduct->save();


        } catch (\Exception $e) {
            $this->_getSession()->addError($e->getMessage());

            $this->_redirectError($redirectUrl);
        }

        Mage::dispatchEvent(
            'frontendproduct_payment_success',
            array(
                'product' => $frontendProduct->getProduct(),
                'customer' => $session->getCustomer()
            )
        );

        $successUrl = Mage::getUrl('product/index/edit/id/' . $frontendProduct->getProduct()->getId(), array('_secure' => true));

        $this->_getSession()->addSuccess('Product save successful');
        $this->_redirectSuccess($successUrl);
    }

    /**
     * Customer product manage
     */
    public function manageAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');

        $this->getLayout()->getBlock('head')->setTitle($this->__('My Adverts'));

        if ($block = $this->getLayout()->getBlock('customer.account.link.back')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $this->renderLayout();

    }

    public function editAction()
    {
        $frontendProduct = $this->_initFrontendProduct();
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');

        $this->getLayout()->getBlock('head')->setTitle($this->__('Edit Advert'));

        $this->getLayout()->getBlock('hunters_frontendproduct_edit')
            ->setProduct($frontendProduct->getProduct())
            ->setCurrentAmount($frontendProduct->getCurrentAmount())
        ;

        if((int)$frontendProduct->getProduct()->getId() < 1) {
            $this->getLayout()->getBlock('hunters_frontendproduct_edit')->setTemplate('frontendproduct/noproduct.phtml');
        }



        if ($block = $this->getLayout()->getBlock('customer.account.link.back')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $this->renderLayout();
    }

    /**
     * product add action
     */
    public function addfrontAction() {

        $redirectUrl = Mage::getUrl('product/index/manage/', array('_secure' => true));

        if (!$this->_validateFormKey()) {
            $this->_redirectError($redirectUrl);
            return;
        }

        /** @var $session Mage_Customer_Model_Session */
        $session = $this->_getSession();
        if ( false === (bool)$session->isLoggedIn() ) {
            $this->_redirect('*/*/');
            return;
        }

        if (!$this->getRequest()->isPost()) {
            $this->_redirectError($redirectUrl);
            return;
        }

        if(false === $this->validateTermsAgreement()) {
            $this->_getSession()->addError('You should accept term and conditions to continue');
            $this->_redirectError($redirectUrl);
            return;
        }

        $frontendProduct = Mage::getModel('hunters_frontendproduct/frontendProduct');

        try {

            $frontendProduct->setDataFromRequest($this->getProductData());
            $frontendProduct->setImageDataFromRequest($this->uploadImages());

            $frontendProduct->createProduct();


        } catch (\Exception $e) {
            $this->_getSession()->addError($e->getMessage());

            $this->_redirectError($redirectUrl);
        }

        Mage::dispatchEvent(
            'frontendproduct_create_success',
            array(
                'product' => $frontendProduct->getProduct(),
                'customer' => $session->getCustomer()
            )
        );

        $this->_getSession()->addSuccess('Product submition successful.');
        $this->_redirectSuccess($redirectUrl);
    }


    /**
     * product add action
     */
    public function editfrontAction() {

        $redirectUrl = Mage::getUrl('product/index/edit/', array('_secure' => true));

        if (!$this->_validateFormKey()) {
            $this->_redirectError($redirectUrl);
            return;
        }

        /** @var $session Mage_Customer_Model_Session */
        $session = $this->_getSession();
        if ( false === (bool)$session->isLoggedIn() ) {
            $this->_redirect('*/*/');
            return;
        }

        if (!$this->getRequest()->isPost()) {
            $this->_redirectError($redirectUrl);
            return;
        }

        if(false === $this->validateTermsAgreement()) {
            $this->_getSession()->addError('You should accept term and conditions to continue');
            $this->_redirectError($redirectUrl);
            return;
        }

        $frontendProduct = Mage::helper('hunters_frontendproduct')->initFrontendProduct();

        if((int)$frontendProduct->getProduct()->getId() < 1) {
            $this->_getSession()->addError('Invalid product to edit');
            $this->_redirectError($redirectUrl);
            return;
        }

        try {

            $frontendProduct->setDataFromRequest($this->getProductData());
            $frontendProduct->setImageDataFromRequest($this->uploadImages());

            $frontendProduct->updateProduct();


        } catch (\Exception $e) {
            $this->_getSession()->addError($e->getMessage());

            $this->_redirectError($redirectUrl);
        }

        Mage::dispatchEvent(
            'frontendproduct_edit_success',
            array(
                'product' => $frontendProduct->getProduct(),
                'customer' => $session->getCustomer()
            )
        );

        $successUrl = Mage::getUrl('product/index/edit/id/' . $frontendProduct->getProduct()->getId(), array('_secure' => true));

        $this->_getSession()->addSuccess('Product save successful');
        $this->_redirectSuccess($successUrl);
    }

    /**
     *      * TODO
     * @deprecated
     * use store credit logic
     * @param $amount
     * @return bool
     */
    protected function checkCustomerBalance($amount)
    {
        return true;
    }

    /**
     * TODO
     * validate data
     * @return array
     * @throws Exception
     */
    protected function getProductDataFromRequest()
    {
        $productData = array();
        $userData = Mage::app()->getRequest()->getParam('product');

        if(count($userData) < 1) {
            throw new \Exception('Empty data');
        }

        foreach ($userData as $key=>$val){

            if(empty(trim($val))) continue;

            $productData[$key] = $val; //VALIDATE
        }

        if(count($userData) < 1) {
            throw new \Exception('No data to load');
        }

        return $productData;
    }

    /**
     * @return array
     * @throws Exception
     */
    protected function getCategoriesFromRequest()
    {
        $userData = Mage::app()->getRequest()->getParam('categories');

        $categoryIds = array();


        if(is_array($userData)) {
            foreach($userData as $categoryId) {
                $id = (int)$categoryId;

                if($id > 0) $categoryIds[] = $id;

            }
        } else {
            $id = (int)$userData;

            if($id > 0) $categoryIds[] = $id;
        }

        if(count($categoryIds) < 1) {
            throw new \Exception('Wrong category id');
        }

        return array('category_ids' => $categoryIds);
    }

    /**
     * @return array
     * @throws Exception
     */
    protected function getProductData()
    {
        $productData = array_merge(
            $this->getProductDataFromRequest(),
            $this->getCategoriesFromRequest()
        );

        return $productData;

    }

    /**
     * Retrieve customer session model object
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * @return bool
     */
    protected function validateTermsAgreement()
    {
        return (bool)$this->getRequest()->getParam('terms', null);
    }

    public function uploadImages()
    {

        $imageGallery = array();

        foreach($this->getProductImagesFromRequest() as $images){

            try {
                $uploader = new Mage_Core_Model_File_Uploader($images);
                $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                $uploader->addValidateCallback('catalog_product_image',
                    Mage::helper('catalog/image'), 'validateUploadFile');
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $result = $uploader->save(
                    Mage::getSingleton('catalog/product_media_config')->getBaseTmpMediaPath()
                );

                $imageGallery[] = $result;

                Mage::dispatchEvent('catalog_product_gallery_upload_image_after', array(
                    'result' => $result,
                    'action' => $this
                ));

            } catch (Exception $e) {

            }

            return $imageGallery;

        }



    }

    protected function getProductImagesFromRequest()
    {
        $imagesData = array();
        $images = $_FILES['images'];

        if(count($images) < 1) {
            return $imagesData;
        }

        foreach ($images['tmp_name'] as $key=>$val){

            if(empty(trim($val['']))) continue;

            $imagesData[$key]['name'] = $images['name'][$key];
            $imagesData[$key]['type'] = $images['type'][$key];
            $imagesData[$key]['error'] = $images['error'][$key];
            $imagesData[$key]['size'] = $images['size'][$key];
            $imagesData[$key]['tmp_name'] = $images['tmp_name'][$key];

        }

        return $imagesData;
    }

    protected function uploadProductImages()
    {
        $imagePath = Mage::getBaseDir('media') . '/catalog/product/images/' . $image;
        $imageResized = Mage::getBaseDir('media') . '/catalog/product/compress/' . $image;


        if (!file_exists($imageResized) && file_exists($imagePath)) {
            $imageObj = new Varien_Image($imagePath);
            $imageObj->constrainOnly(TRUE);
            $imageObj->keepAspectRatio(TRUE);
            $imageObj->keepFrame(FALSE);
            $imageObj->resize(600);
            $imageObj->save($imageResized);

            /*            */
        }
    }

}
