<?php


class Hunters_FrontendProduct_Block_Manage extends Mage_Core_Block_Template
{

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('frontendproduct/list.phtml');

        $frontendproductAliasName = 'fp';

        $products = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('price')
            ->addAttributeToSelect('status')
            ->addAttributeToSelect('visibility')
            ->addAttributeToFilter('type_id', array('eq' => 'virtual'));

        $products->getSelect()
            ->join(
                array(
                    'fp' => $products->getTable('hunters_frontendproduct/frontendproduct')),
                "e.entity_id = {$frontendproductAliasName}.product_id and {$frontendproductAliasName}.customer_id = " . Mage::getSingleton('customer/session')->getCustomer()->getId(),
                array(
                    $frontendproductAliasName . '.entity_id as fp_id',
                    $frontendproductAliasName . '.current_amount',
                    $frontendproductAliasName . '.status',
                    $frontendproductAliasName . '.updated_at as fp_apdated_at',

                )
            );
        $products->setOrder('updated_at', 'desc');


        $this->setProducts($products);

       // Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('root')->setHeaderTitle(Mage::helper('sales')->__('My Adverts'));
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'sales.order.history.pager')
            ->setCollection($this->getProducts());
        $this->setChild('pager', $pager);
        $this->getProducts()->load();
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }

    public function getEditUrl()
    {
        return $this->getUrl('product/index/edit/');
    }
}
