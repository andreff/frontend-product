<?php

/**
 * Class Hunters_FrontendProduct_Model_Resource_FrontendProduct
 */

class Hunters_FrontendProduct_Model_Resource_Frontendproduct  extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('hunters_frontendproduct/frontendproduct', 'entity_id');
    }

}