<?php

/**
 * Class Hunters_FrontendProduct_Model_System_Config_Source_Required
 */
class Hunters_FrontendProduct_Model_System_Config_Source_Required
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'name', 'label'=>Mage::helper('hunters_frontendproduct')->__('Name')),
            array('value' => 'sku', 'label'=>Mage::helper('hunters_frontendproduct')->__('SKU')),
            array('value' => 'description', 'label'=>Mage::helper('hunters_frontendproduct')->__('Description')),
            array('value' => 'short_description', 'label'=>Mage::helper('hunters_frontendproduct')->__('Short Description')),
            array('value' => 'weight', 'label'=>Mage::helper('hunters_frontendproduct')->__('Weight')),
            array('value' => 'status', 'label'=>Mage::helper('hunters_frontendproduct')->__('Status')),
            array('value' => 'url_key', 'label'=>Mage::helper('hunters_frontendproduct')->__('URL Key')),
            array('value' => 'visibility', 'label'=>Mage::helper('hunters_frontendproduct')->__('Visibility')),
            array('value' => 'price', 'label'=>Mage::helper('hunters_frontendproduct')->__('Price')),
            array('value' => 'tax_class', 'label'=>Mage::helper('hunters_frontendproduct')->__('Tax Class')),
            array('value' => 'meta_title', 'label'=>Mage::helper('hunters_frontendproduct')->__('Meta Title')),
            array('value' => 'meta_keyword', 'label'=>Mage::helper('hunters_frontendproduct')->__('Meta Keywords')),
            array('value' => 'meta_description', 'label'=>Mage::helper('hunters_frontendproduct')->__('Meta Description')),
            array('value' => 'qty', 'label'=>Mage::helper('hunters_frontendproduct')->__('Qty')),
            array('value' => 'manage_stock', 'label'=>Mage::helper('hunters_frontendproduct')->__('Manage Stock')),
            array('value' => 'is_in_stock', 'label'=>Mage::helper('hunters_frontendproduct')->__('Is In Stock')),
            array('value' => 'categories', 'label'=>Mage::helper('hunters_frontendproduct')->__('Categories')),
            array('value' => 'websites', 'label'=>Mage::helper('hunters_frontendproduct')->__('Websites')),
            array('value' => 'image', 'label'=>Mage::helper('hunters_frontendproduct')->__('Image')),
        );
    }
}