<?php
/**
 * Created by PhpStorm.
 * User: andreff
 * Date: 25.04.17
 * Time: 15:54
 */

class Hunters_FrontendProduct_Model_Product extends Mage_Catalog_Model_Product
{

    /**
     * Check product options and type options and save them, too
     */
    protected function _beforeSave()
    {

        $this->getCategoryIdsBeforeSave();

        $this->saveProductLocation();

        parent::_beforeSave();

    }


    protected function getCategoryIdsBeforeSave()
    {

        $categories =  $this->getCategoryIds();

        if(count($categories) == 1){

            $categoryId = array_shift($categories);

            $newCategoryIds = Mage::helper('hunters_frontendproduct/category')->loadParentCategoryIds($categoryId);

            $ids = array_merge($this->getData('category_ids'),$newCategoryIds);

            $this->setCategoryIds($ids);
        }

        return $this;
    }


    /**
     * Update Address, City, Country
     * @return $this
     */
    protected function saveProductLocation()
    {

        if((bool)$this->getLatitude() === false || (bool)$this->getLongitude() === false) {
            return $this;
        }

        $address = Mage::helper('russifieds_catalog/productlocator')->getAddressFromGoogleApis($this->getLatitude(),$this->getLongitude());

        $this->setAddress($address->getAddress());
        $this->setCity($address->getCity());
        $this->setCountry($address->getCountry());


        return $this;
    }
}