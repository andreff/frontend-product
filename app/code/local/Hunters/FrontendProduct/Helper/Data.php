<?php

/**
 * Class Hunters_FrontendProduct_Helper_Data
 */
class Hunters_FrontendProduct_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getAddLinkLabel() {
        return $this->__('Подать объявление');
    }
    public function getManageLinkLabel() {
        return $this->__('Управление объявлениями');
    }

    /**
     * Initialize product from request parameters
     *
     * @return Mage_Catalog_Model_Product
     */
    public function initFrontendProduct()
    {

        $productId  = (int) Mage::app()->getRequest()->getParam('id');
        $frontendProduct = Mage::getModel('hunters_frontendproduct/frontendProduct');
        $product = $frontendProduct->getProduct();

        if ($productId) {
            try {
                $frontendProduct->loadByProductId($productId);
                $product->load($productId);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }

        return $frontendProduct;
    }

}
