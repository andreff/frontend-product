<?php

/**
 * Class Hunters_FrontendProduct_Helper_Category
 */
class Hunters_FrontendProduct_Helper_Category extends Mage_Core_Helper_Abstract
{
    /**
     * @param $categoryId
     * @return array
     */
    public function loadParentCategoryIds($categoryId)
    {
        $newCategories = array();

        $catId = $categoryId;
        $i = 0;
        while ($i < 10){

            $i++;

            $catId = $this->getParentCategoryId(Mage::getModel('catalog/category')->load($catId));

            if ((bool)$catId === false) {
                $i = 10;
            } else {
                $newCategories[] = $catId;
            }

        }


        return $newCategories;

    }

    /**
     * @param Mage_Catalog_Model_Category $category
     * @return bool|mixed
     */
    public function getParentCategoryId(Mage_Catalog_Model_Category $category)
    {

        $parentId = $category->getParentCategory()->getId();

        if($category->getId() == $parentId) {
            return false;
        }
        if($parentId == 1) {
            return false;
        }

        return $parentId;
    }

}