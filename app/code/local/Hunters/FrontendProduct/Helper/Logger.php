<?php

/**
 * Class Hunters_FrontendProduct_Helper_Logger
 */
class Hunters_FrontendProduct_Helper_Logger extends Mage_Core_Helper_Abstract
{

    const LOG_FORCE = false;

    /**
     * @param $message
     * @param int $level
     * @param bool $force\
     */
    public function log($message, $level = Zend_Log::DEBUG)
    {
        if($this->isEnabled()) {

            Mage::log($message, $level, $this->getLogFileName(), true);

        }
    }

    public function logException(Exception $e)
    {
        Mage::log($e->getMessage(), Zend_Log::CRIT, $this->getLogFileName(), true);

        Mage::logException($e);

        $this->notifyAdmin($e->getMessage());
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return (bool)Mage::getStoreConfig('hunters_frontendproduct/general/debug');
    }

    /**
     * @return mixed
     */
    protected function getLogFileName()
    {
        return Mage::getStoreConfig('hunters_frontendproduct/general/log_file_name');
    }

    /**
     * send admin mail
     * @return bool
     * TODO
     */
    protected function notifyAdmin($message)
    {
        return true;
    }


}