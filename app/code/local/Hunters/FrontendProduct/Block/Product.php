<?php

class  Hunters_FrontendProduct_Block_Product extends Mage_Core_Block_Template
{

    /**
     * @return bool
     */
    public function getProductId()
    {
        $productId = false;

        if($this->getProduct() instanceof Mage_Catalog_Model_Product) {
            $productId = $this->getProduct()->getId();
        }

        return $productId;
    }

    /**
     * @return bool
     */
    public function productIsLoaded()
    {
        if((int)$this->getProductId() > 0) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @return mixed|Varien_Object
     */
    public function getProduct()
    {

        if($this->getData('product') instanceof Mage_Catalog_Model_Product) {
            return $this->getData('product');
        } else {
            return new Varien_Object();
        }
    }

    /**
     * @return array
     */
    public function getProductAdTypeOptions()
    {

        $options = array();

        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'ad_type');

        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
        }

        return $options;
    }

    /**
     * @return string
     */
    public function getAddFormUrl()
    {
        return $this->getUrl('product/index/addfront');
    }

    /**
     * @return string
     */
    public function getEditFormUrl()
    {
        return $this->getUrl('product/index/editfront');
    }

    /**
     * @return string
     */
    public function getPaymentFormActionUrl()
    {
        return $this->getUrl('product/index/payment');
    }

    /**
     * @return string
     */
    public function getFormActionUrl()
    {
        $url = $this->getAddFormUrl();
        if($this->productIsLoaded() === true) {

            $url = $this->getEditFormUrl();

        }

        return $url;
    }

    /**
     * @param $categoryId
     * @return string|void
     */
    public function categorySelected($categoryId)
    {
        $catids = $this->getProduct()->getCategoryIds();

        if(count($catids)< 1) return;

        if(in_array($categoryId,$catids)) {
            return 'selected="selected"';
        }
    }

    /**
     * @param $optionValue
     * @param $currentValue
     * @return string
     */
    public function getOptionSelectedText($optionValue,$currentValue)
    {
        if($optionValue == $currentValue) {
            return 'selected="selected"';
        } else {
            return '';
        }
    }
}