<?php
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer = $this;

$installer->startSetup();

/**
 * Create table 'hunters_frontendproduct/frontendproduct'
 */

$table = $installer->getConnection()
    ->newTable($installer->getTable('hunters_frontendproduct/frontendproduct'))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Crosstable Id')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Customer Id')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Advert/Product Id')
    ->addColumn('current_amount', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12,4), array(
        'nullable'  => false,
        'default'   => 0.0000,
    ), 'Current Amount')
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
    ), 'Updated at date')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '0',
    ), 'Status')

    ->setComment('Frontend Product Customer Balance');

if ($installer->getConnection()->isTableExists($table) === false) {
    $installer->getConnection()->createTable($table);
}