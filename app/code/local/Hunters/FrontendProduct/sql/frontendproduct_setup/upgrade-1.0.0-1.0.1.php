<?php
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', 'latitude', array(
    'group'         => 'General',
    'type'          => 'varchar',
    'backend'       => '',
    'frontend'      => '',
    'label'         => 'Latitude',
    'input'         => 'text',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       => true,
    'required'      => false,
    'user_defined'  => false,
    'is_visible_on_front' => true
));


$installer->addAttribute('catalog_product', 'longitude', array(
    'group'         => 'General',
    'type'          => 'varchar',
    'backend'       => '',
    'frontend'      => '',
    'label'         => 'Longitude',
    'input'         => 'text',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       => true,
    'required'      => false,
    'user_defined'  => false,
    'used_in_product_listing'  => true,
    'is_visible_on_front' => true
));


$installer->addAttribute('catalog_product', 'address', array(
    'group'         => 'General',
    'type'          => 'varchar',
    'backend'       => '',
    'frontend'      => '',
    'label'         => 'Address',
    'input'         => 'text',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       => true,
    'required'      => false,
    'user_defined'  => false,
    'used_in_product_listing'  => true,
    'is_visible_on_front' => true
));

$installer->addAttribute('catalog_product', 'city', array(
    'group'         => 'General',
    'type'          => 'varchar',
    'backend'       => '',
    'frontend'      => '',
    'label'         => 'City',
    'input'         => 'text',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       => true,
    'required'      => false,
    'user_defined'  => false,
    'used_in_product_listing'  => true,
    'is_visible_on_front' => true
));

$installer->addAttribute('catalog_product', 'country', array(
    'group'         => 'General',
    'type'          => 'varchar',
    'backend'       => '',
    'frontend'      => '',
    'label'         => 'Country',
    'input'         => 'text',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       => true,
    'required'      => false,
    'user_defined'  => false,
    'used_in_product_listing'  => true,
    'is_visible_on_front' => true
));